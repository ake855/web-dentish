import { Component, OnInit } from '@angular/core';
import { SlotService } from './calendar.service';
import { DateTime } from 'luxon';
import { JwtHelperService } from '@auth0/angular-jwt';

// @fullcalendar plugins
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import timeGridPlugin from '@fullcalendar/timegrid';
import { CalendarOptions } from '@fullcalendar/core';
import { LookupService } from 'src/app/backend/services/lookup.service';


@Component({
    templateUrl: './calendar.app.component.html',
    styleUrls: ['./calendar.app.component.scss'],
})
export class CalendarAppComponent implements OnInit {

    jwtHelper: JwtHelperService = new JwtHelperService();
    events: any[] = [];

    today: string = '';

    calendarOptions: any;

    showDialog: boolean = false;

    clickedEvent: any = null;

    dateClicked: boolean = false;

    edit: boolean = false;

    tags: any[] = [];

    view: string = '';

    changedEvent: any;
    data: any = [
        {
            "id": 1,
            "title": "Maecenas Pulvinar",
            "start": "2024-01-10",
            "end": "2024-01-10",
            "description": "eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus",
            "location": "260 Calypso Terrace",
            "backgroundColor": "#FFB6B6",
            "borderColor": "#FFB6B6",
            "textColor": "#212121",
            "tag": { "color": "#FFB6B6", "name": "Company A" }
        },
        {
            "id": 2,
            "title": "Aenean Lectus",
            "start": "2024-01-10",
            "end": "2024-01-10",
            "description": "Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.",
            "location": "93529 Sloan Junction",
            "backgroundColor": "#FFC7E8",
            "borderColor": "#FFC7E8",
            "textColor": "#212121",
            "tag": { "color": "#FFC7E8", "name": "Company B" }
        },
        {
            "id": 3,
            "title": "Nam Ultrices",
            "start": "2024-01-11",
            "end": "2024-01-11",
            "description": "Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.",
            "location": "9295 Almo Avenue",
            "backgroundColor": "#D2D6FF",
            "borderColor": "#D2D6FF",
            "textColor": "#212121",
            "tag": { "color": "#D2D6FF", "name": "Company C" }
        },
    ]

    now = DateTime.now();

    selectServiceTypeId: Number;
    listSlot: any = [];
    listHospitals: any = [];
    listPeriods: any = [];
    listCustomers: any = [];

    isLoading: boolean = false;

    customer: any = {};
    customerName: string = '';
    customerTel: string = '';
    customerLineId: string = '';
    customerAge: string = '';
    customerCID: string = '';
    customerPassport: string = '';
    customerChronics: string = '';

    userId: Number;

    constructor(
        private slotService: SlotService, private lookupService: LookupService
    ) {
        this.selectServiceTypeId = Number(
            sessionStorage.getItem('serviceTypeId')
        );
        this.userId = Number(sessionStorage.getItem('userId'));
        this.listSlot = JSON.parse(sessionStorage.getItem('slots') || '[]');
    }

    ngOnInit(): void {
        this.today = this.now.setLocale('th-TH').toISODate();

        this._prepareCalendar();
        this._prepareHospital();
        this.getCustomer();
    }


    async getCustomer() {
        const userId: any = sessionStorage.getItem('userId');
        const res: any = await this.lookupService.listCustomerByID(userId);
        let customer = [];
        customer = res.data;
        if (customer.ok) {
            this.listCustomers = customer.results[0];
            console.log(this.listCustomers);
            
            this.customerCID=this.listCustomers.customer_name;

        } else {
            alert('error load period')
        }


    }

    _prepareHospital() {
        this.listHospitals = Array.from(
            new Set(this.listSlot.map((event: any) => event.hospital_id))
        ).map((hospital_id: any) => {
            return this.listSlot.find((event: any) => event.hospital_id === hospital_id);
        });
        console.log('hospital:', this.listHospitals);
    }

    selectHospital(hospital_id: any) {
        this.listPeriods = this.listSlot.filter(
            (event: any) => event.hospital_id === hospital_id
        );
        console.log('periods:', this.listPeriods);
    }

    async _prepareCalendar() {
        let events = await this.listSlot.map((event: any) => ({
            title: event.slot_name,
            start: DateTime.fromISO(event.slot_date)
                .setLocale('th-TH')
                .toISODate(),
            display: 'background',
            backgroundColor: '#90F871',
            borderColor: '#90F871',
            textColor: '#010101',
            id: event.slot_id,
        }));

        this.calendarOptions = {
            plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
            height: 320,
            initialView: 'dayGridMonth',
            initialDate: this.today,
            headerToolbar: {
                left: 'prev',
                center: 'title',
                right: 'next',
            },
            locale: 'th-TH',
            editable: true,
            selectable: true,
            selectMirror: true,
            dayMaxEvents: true,
            events: events,
            selectLongPressDelay: 100, //ค่าเร่ิมต้น สำหรับ teblet mobile  = 1000ms  คือต้องกดค้าง 1000 ms
            eventClick: (e: MouseEvent) => this.onEventClick(e),
            select: (e: MouseEvent) => this.onDateSelect(e),
        };
    }
    handleDayRender(arg: any) {
        console.log(arg);

        // Check if there is an event for the current date
        const hasEvent = this.data.some(
            (event: any) => event.start === arg.date.toISOString().split('T')[0]
        );

        // If no event, disable the day
        if (!hasEvent && arg.el) {
            console.log(arg);
            arg.el.classList.add('fc-day-disabled');
            // console.log('no event');
        }
    }

    isHoliday(date: Date): boolean {
        // Implement your logic to determine if the date is a holiday
        // Replace this with your own holiday detection algorithm
        // For example, you might have a list of holiday dates to check against
        const holidayDates = ['2024-01-10'];
        return holidayDates.some((holiday) => date.toISOString().split('T')[0] === holiday);
    }


    onEventClick(e: any) {
        this.clickedEvent = e.event;
        let plainEvent = e.event.toPlainObject({
            collapseExtendedProps: true,
            collapseColor: true,
        });
        this.view = 'display';
        this.showDialog = true;

        this.changedEvent = { ...plainEvent, ...this.clickedEvent };
        this.changedEvent.start = this.clickedEvent.start;
        this.changedEvent.end = this.clickedEvent.end
            ? this.clickedEvent.end
            : this.clickedEvent.start;
    }

    onDateSelect(e: any) {
        this.view = 'new';
        this.showDialog = true;
        this.changedEvent = {
            ...e,
            title: null,
            description: null,
            location: null,
            backgroundColor: null,
            borderColor: null,
            textColor: null,
            tag: { color: null, name: null },
        };
    }

    handleSave() {
        if (!this.validate()) {
            return;
        } else {
            this.showDialog = false;
            let data = {
                customer_name: this.customerName,
                phone_number: this.customerTel,
                line_id: this.customerLineId,
                age: this.customerAge,
                cid: this.customerCID,
                passport: this.customerPassport,
                chronics: this.customerChronics,
            };
            this.customer = data;
            this.save(this.customer);

            this.clickedEvent = {
                ...this.changedEvent,
                backgroundColor: this.changedEvent.tag.color,
                borderColor: this.changedEvent.tag.color,
                textColor: '#212121',
            };

            if (this.clickedEvent.hasOwnProperty('id')) {
                this.events = this.events.map((i) =>
                    i.id.toString() === this.clickedEvent.id.toString()
                        ? (i = this.clickedEvent)
                        : i
                );
            } else {
                this.events = [
                    ...this.events,
                    {
                        ...this.clickedEvent,
                        id: Math.floor(Math.random() * 10000),
                    },
                ];
            }
            this.calendarOptions = {
                ...this.calendarOptions,
                ...{ events: this.events },
            };
            this.clickedEvent = null;
        }
    }

    onEditClick() {
        this.view = 'edit';
    }

    delete() {
        this.events = this.events.filter(
            (i) => i.id.toString() !== this.clickedEvent.id.toString()
        );
        this.calendarOptions = {
            ...this.calendarOptions,
            ...{ events: this.events },
        };
        this.showDialog = false;
    }

    validate() {
        let { start, end } = this.changedEvent;
        return start && end;
    }


    async save(customer: any) {
        try {
            const res: any = await this.slotService.saveCustomer(customer);
            let data = res.data;
            if (data.ok) {
                const customerId = data.results.customer_id;
                console.log('saveCustomer:', data.results);
                this.saveReserves(customerId);
            }
        } catch (error) {
            console.log(error);
        }
    }

    async saveReserves(id: any) {
        let reserve = {
            customer_id: id,
            slot_id: this.clickedEvent.id,
            user_id: this.userId,
            service_type_id: this.selectServiceTypeId,
            reserve_date: this.today,
        };
        try {
            const res: any = await this.slotService.saveReserves(reserve);
            let data = res.data;
            if (data.ok) {
                console.log('saveReserves:', data.results);
            }
        } catch (error) {
            console.log(error);
        }
    }
}
