import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { HomeService } from 'src/app/backend/components/home/home.service';
import { Table } from 'primeng/table';
import * as _ from 'lodash';

@Component({
    selector: 'app-backend-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    // breadcrumb items
    blockedPanel: boolean = false;
    home: any;
    homeData: any = [];
    loading: boolean = true;
    slot_type: any = [];
    all_data: any;
    dataLookupSlot: any = [];
    data_name:any;

    @ViewChild('filter') filter!: ElementRef;
    all_data1: any;
    constructor(
        private layoutService: LayoutService,
        private homeService: HomeService
    ) {
    }

    async ngOnInit() {
        await this.lookupSlot();
        await this.getData();
        this.loading = false;
    }

    // get Services data from API
    async getData() {

        this.blockedPanel = true;
        let hospitalID: any = sessionStorage.getItem('hospitalId');
        let serviceID: any = sessionStorage.getItem('serviceId');
        try {
            const res: any = await this.homeService.list(hospitalID, serviceID);
            let all_data = [];
            let data_reserve:any = [];
            all_data = res.data;
            console.log("---รีเทินจาก api", res);
            if (all_data.ok) {

                this.all_data = all_data.results[0].reserve[0];
                data_reserve.customer = [all_data.results[0].reserve[0].customer];
               
                this.data_name = data_reserve.customer;
               
                console.log("ค่าที่ส่งไปหน้า HTML", this.all_data);
                console.log("ค่าที่ส่งไปหน้า HTML",  data_reserve);



            } else {
                alert('Error loading DATA');
            }
            this.blockedPanel = false;
        } catch (error) {
            this.blockedPanel = false;
            console.log(error);
        }

    }


    async lookupSlot() {

        const res: any = await this.homeService.lookupSlot();
        console.log("---รีเทินจาก lookupslot", res);
        if (res.data.ok) {
            this.dataLookupSlot = [res.data.results];
            //console.log("---รีเทินจาก lookupslot ทบทวน", this.dataLookupSlot);


        } else {
            alert('Error loading DATA');
        }




    }

    onGlobalFilter(table: Table, event: Event) {

        table.filterGlobal((event.target as HTMLInputElement).value, 'contains');

    }

    clear(table: Table) {
        table.clear();
        this.filter.nativeElement.value = '';
    }

    scrollTo(viewChild: HTMLElement) {
        viewChild.scrollIntoView({ behavior: 'smooth' });
    }

    get backgroundStyle(): object {
        let path = 'assets/demo/images/landing/';
        let image = this.layoutService.config.colorScheme === 'dark' ? 'line-effect-dark.svg' : 'line-effect.svg';

        return { 'background-image': 'url(' + path + image + ')' };
    }

    get colorScheme(): string {
        return this.layoutService.config.colorScheme;
    }
}